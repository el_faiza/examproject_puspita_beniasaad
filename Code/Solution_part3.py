# import the libraries
import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import sqlite3

input1 = os.path.abspath("../Results/Results_Part1/output_part1.csv")
input2 = os.path.abspath("../Results/Results_Part2/output_part2.csv")
input3 = os.path.abspath("../Data/part3.csv")

df0 = pd.read_csv(input1)
df1 = pd.read_csv(input2)
df2 = pd.read_csv(input3)

df3 = pd.merge(df1, df2, on='CLIENT_ID')
df4 = pd.merge(df0, df1, on='CLIENT_ID')
df5 = pd.merge(df0, df1[['CLIENT_ID', 'Client_label']], on='CLIENT_ID')

# Comparing labels
df3["CORRECT_PREDICTION"] = (df3["Client_label"] == df3["CLIENT_TYPE"])
# print(df3["CORRECT_PREDICTION"])

# Calculate percentage of correct prediction
percent_correct = df3.CORRECT_PREDICTION.value_counts().loc[True] / len(df3["CORRECT_PREDICTION"])
print("The correct prediction from Part 2 calculation is: " + "{0:.2%}".format(percent_correct))

#The distribution of clients : Solution 1
Client_distri = df1.groupby(['Client_label']).size().reset_index(name='Clients_Counts')
Client_distri['Probability'] = [i/Client_distri['Clients_Counts'].sum() for i in Client_distri['Clients_Counts']]
print(Client_distri)
#The distribution of clients : Solution 2
print(pd.crosstab(df1['TIME'],df1['Client_label'],normalize='index'))
#The distribution of clients : Solution 3
print(df1.groupby('Client_label').size().div(len(df1)))

#The likehood to get first course
subset_df1 = df1[df1["FIRST_COURSE"] > 0.0].groupby(['Client_label']).size().reset_index(name='Starter_Counts')
subset_df1['Starter_Probability'] = [i/j for i, j in zip(subset_df1['Starter_Counts'], Client_distri['Clients_Counts'])]
#The likehood to get second course
subset_df2 = df1[df1["SECOND_COURSE"] > 0.0].groupby(['Client_label']).size().reset_index(name='Main_Counts')
subset_df2['Menu_Probability'] = [i/j for i, j in zip(subset_df2['Main_Counts'], Client_distri['Clients_Counts'])]
#The likehood to get third course
subset_df3 = df1[df1["THIRD_COURSE"] > 0.0].groupby(['Client_label']).size().reset_index(name='Dessert_Counts')
subset_df3['Dessert_Probability'] = [i/j for i, j in zip(subset_df3['Dessert_Counts'], Client_distri['Clients_Counts'])]
#The likelihood for each of the clients type to get a certain course
Client_Course = pd.merge(pd.merge(subset_df1, subset_df2, on='Client_label'), subset_df3, on='Client_label')
print(Client_Course)

#First course distribution
Soup_Counts = df4[df4["FOOD_FIRST_COURSE"] == 3.0].groupby(['Client_label']).size().reset_index(name='Soup_Counts')
TomatoMozarella_Counts = df4[df4["FOOD_FIRST_COURSE"] == 15.0].groupby(['Client_label']).size().reset_index(name='TomatoMozarella_Counts')
Oysters_Counts = df4[df4["FOOD_FIRST_COURSE"] == 20.0].groupby(['Client_label']).size().reset_index(name='Oysters_Counts')
First_course = pd.merge(pd.merge(Soup_Counts, TomatoMozarella_Counts, on='Client_label', how='outer'), Oysters_Counts, on='Client_label', how='outer')
First_course.index = First_course['Client_label'].values
First_course = First_course.reindex(["Business", "Healthy", "Onetime", "Retirement"]).reset_index().drop(columns = ['index'])
dishes_count = []  # Used for next question
dishes_count.append(First_course.copy())  # Used for next question
First_course['Soup_Probability'] = [i/j for i, j in zip(First_course['Soup_Counts'], subset_df1['Starter_Counts'])]
First_course['TomatoMozarella_Probability'] = [i/j for i, j in zip(First_course['TomatoMozarella_Counts'], subset_df1['Starter_Counts'])]
First_course['Oysters_Probability'] = [i/j for i, j in zip(First_course['Oysters_Counts'], subset_df1['Starter_Counts'])]
#Second course distribution
Salad_Counts = df4[df4["FOOD_SECOND_COURSE"] == 9.0].groupby(['Client_label']).size().reset_index(name='Salad_Counts')
Spaghetti_Counts = df4[df4["FOOD_SECOND_COURSE"] == 20.0].groupby(['Client_label']).size().reset_index(name='Spaghetti_Counts')
Steak_Counts = df4[df4["FOOD_SECOND_COURSE"] == 25.0].groupby(['Client_label']).size().reset_index(name='Steak_Counts')
Lobster_Counts = df4[df4["FOOD_SECOND_COURSE"] == 40.0].groupby(['Client_label']).size().reset_index(name='Lobster_Counts')
Second_course = pd.merge(pd.merge(pd.merge(Salad_Counts, Spaghetti_Counts, on='Client_label', how='outer'), Steak_Counts, on='Client_label', how='outer'), Lobster_Counts, on='Client_label', how='outer')
Second_course.index = Second_course['Client_label'].values
Second_course = Second_course.reindex(["Business", "Healthy", "Onetime", "Retirement"]).reset_index().drop(columns = ['index'])
dishes_count.append(Second_course.copy())  # Used for next question
Second_course['Salad_Probability'] = [i/j for i, j in zip(Second_course['Salad_Counts'], subset_df2['Main_Counts'])]
Second_course['Spaghetti_Probability'] = [i/j for i, j in zip(Second_course['Spaghetti_Counts'], subset_df2['Main_Counts'])]
Second_course['Steak_Probability'] = [i/j for i, j in zip(Second_course['Steak_Counts'], subset_df2['Main_Counts'])]
Second_course['Lobster_Probability'] = [i/j for i, j in zip(Second_course['Lobster_Counts'], subset_df2['Main_Counts'])]
#Third course distribution
IceCream_Counts = df4[df4["FOOD_THIRD_COURSE"] == 15.0].groupby(['Client_label']).size().reset_index(name='IceCream_Counts')
Pie_Counts = df4[df4["FOOD_THIRD_COURSE"] == 10.0].groupby(['Client_label']).size().reset_index(name='Pie_Counts')
Third_course = pd.merge(IceCream_Counts, Pie_Counts, on='Client_label', how='outer')
Third_course.index = Third_course['Client_label'].values
Third_course = Third_course.reindex(["Business", "Healthy", "Onetime", "Retirement"]).reset_index().drop(columns = ['index'])
dishes_count.append(Third_course.copy())  # Used for next question
Third_course['Pie_Probability'] = [i/j for i, j in zip(Third_course['Pie_Counts'], subset_df3['Dessert_Counts'])]
Third_course['IceCream_Probability'] = [i/j for i, j in zip(Third_course['IceCream_Counts'], subset_df3['Dessert_Counts'])]
#The probability of a certain type of customer ordering a certain dish
Client_dish = pd.merge(pd.merge(First_course, Second_course, on='Client_label'), Third_course, on='Client_label')
print(Client_dish)

# Distribution of the dishes
labels = df5['Client_label'].unique()
courses = ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]

i = 0
for course in courses:
    # fig = plt.subplots()
    dish_count = dishes_count[i].fillna(0)
    dftemp = dish_count.set_index('Client_label')
    ax = dftemp.plot.bar(rot=0)
    ax.set_xlabel('Client Label')
    ax.set_title("Distribution of " + course + " dishes")
    ax.figure.savefig(os.path.abspath("../Results/Results_Part3/Distribution of " + course + ".png"))
    i = i + 1

# Distribution of the drinks per course
fig, axs = plt.subplots(ncols=len(courses))
fig.canvas.manager.set_window_title("Distribution Plot for drinks")
i = 0
for course in courses:
    axs[i].set_title(course)
    axs[i].set_xlabel("Price")
    if i == 0:
        axs[i].set_ylabel("Quantity")
    df4[df4["DRINK_" + course] != 0.0]["DRINK_" + course].hist(ax=axs[i])   # Without data with price 0.0
    # df4["DRINK_" + course].hist(ax=axs[i])                                  # With data with dish price 0.0
    i = i + 1
fig.savefig(os.path.abspath("../Results/Results_Part3/Distribution Plot for drinks.png"))
plt.show()
