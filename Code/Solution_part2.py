# import the libraries
import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn import datasets
import seaborn as sns

# Load the cutomers data
input = os.path.abspath("../Data/part1.csv")
outputFile = os.path.abspath("../Results/Results_Part2/output_part2.csv")

# KMeans -- Clustering
df1 = pd.read_csv(input)

# Input matrix for segmentation
x = df1[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']].values

# Find the optimal number of clusters using elbow method
WCSS = []
for i in range(1,11):
    model = KMeans(n_clusters = i,init = 'k-means++')
    model.fit(x)
    WCSS.append(model.inertia_)
fig = plt.figure(figsize = (7,7))
fig.canvas.manager.set_window_title("Optimal number of clusters using elbow method")
plt.plot(range(1,11),WCSS, linewidth=4, markersize=12,marker='o',color = 'red')
plt.xticks(np.arange(11))
plt.xlabel("Number of clusters")
plt.ylabel("WCSS")
plt.show()
fig.savefig(os.path.abspath("../Results/Results_Part2/Optimal_number_clusters.png"))

# since elbow occured at 4, hence the optimal number of clusters as already assumed is 4
# finding the clusters based on input matrix "x"
model = KMeans(n_clusters = 4, init = "k-means++", max_iter = 300, n_init = 10, random_state = 0)
y_clusters = model.fit_predict(x)

# countplot to check the number of clusters and number of customers in each cluster
fig = plt.figure(figsize = (7,7))
fig.canvas.manager.set_window_title("number of customers in each cluster")
sns.countplot(y_clusters)
fig.savefig(os.path.abspath("../Results/Results_Part2/ClientsNumberInclusters.png"))

# 3d scatterplot using matplotlib
fig = plt.figure(figsize = (15,15))
fig.canvas.manager.set_window_title("4 Clusters")
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x[y_clusters == 0,0],x[y_clusters == 0,1],x[y_clusters == 0,2], s = 40 , color = 'blue', label = "cluster 0")
ax.scatter(x[y_clusters == 1,0],x[y_clusters == 1,1],x[y_clusters == 1,2], s = 40 , color = 'orange', label = "cluster 1")
ax.scatter(x[y_clusters == 2,0],x[y_clusters == 2,1],x[y_clusters == 2,2], s = 40 , color = 'green', label = "cluster 2")
ax.scatter(x[y_clusters == 3,0],x[y_clusters == 3,1],x[y_clusters == 3,2], s = 40 , color = '#D12B60', label = "cluster 3")
ax.set_xlabel('First Course')
ax.set_ylabel('Second Course')
ax.set_zlabel('Third Course')
ax.set_title('4 Clusters')
ax.legend()
plt.show()
fig.savefig(os.path.abspath("../Results/Results_Part2/Clusters_Plot.png"))

# Overplot using labels
fig = plt.figure(figsize = (15,15))
fig.canvas.manager.set_window_title("3D KMeans Clustering")
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x[y_clusters == 0,0],x[y_clusters == 0,1],x[y_clusters == 0,2], s = 40 , color = 'blue', label = "Business Clients")
ax.scatter(x[y_clusters == 1,0],x[y_clusters == 1,1],x[y_clusters == 1,2], s = 40 , color = 'orange', label = "Healthy Clients")
ax.scatter(x[y_clusters == 2,0],x[y_clusters == 2,1],x[y_clusters == 2,2], s = 40 , color = 'green', label = "Retirement Clients")
ax.scatter(x[y_clusters == 3,0],x[y_clusters == 3,1],x[y_clusters == 3,2], s = 40 , color = '#D12B60', label = "Normal Clients")
ax.set_xlabel('First Course')
ax.set_ylabel('Second Course')
ax.set_zlabel('Third Course')
ax.set_title('3D KMeans Clustering')
ax.legend()
plt.show()
fig.savefig(os.path.abspath("../Results/Results_Part2/Labeled_Clusters_Plot.png"))

#Adding KMeans labels to data
y_clusters =[4 if k == 0 else k for k in y_clusters]
y_clusters = [str(i) for i in y_clusters]
y_clusters = pd.DataFrame({"column1":y_clusters})
y_clusters["column1"].replace({"4": "Business", "1": "Healthy", "2": "Retirement", "3": "Onetime"}, inplace=True)
df1['Client_label'] = y_clusters["column1"]
df1.to_csv(outputFile, sep=',')

#Characteristics per group
Clients_summary = df1.groupby('Client_label').describe()
print(Clients_summary)

