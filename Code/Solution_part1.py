import pandas as pd
import matplotlib.pyplot as plt
import os
import seaborn as sns
import numpy as np


def get_drink_price(total_paid, dish_price):
    # Find the least non-zero positive subtraction for the drink price
    for x in dish_price:
        drink_price = total_paid - x
        if drink_price > 0:
            return drink_price

    # No non-zero positive subtraction possible, a client pays only for drink or do not take drink at all
    return total_paid


def calculate_drinks(df, courses, prices):
    # Zip makes it possible to iterate over multiple variables
    for course, price in zip(courses, prices):
        df["DRINK_" + course] = df[course].map(lambda x: get_drink_price(x, price))
        df["FOOD_" + course] = df[course] - df["DRINK_" + course]


def create_dis_plot(df, courses):
    fig, axs = plt.subplots(ncols=len(courses))
    fig.canvas.manager.set_window_title("Distribution Plot for each course")
    i = 0
    for course in courses:
        axs[i].set_title(course)
        axs[i].set_xlabel("Price")
        axs[i].set_ylabel("Quantity")
        df[df[course] != 0.0][course].hist(ax=axs[i])
        i = i + 1
    fig.savefig(os.path.abspath("../Results/Results_Part1/Distribution Plot for each course.png"))

def create_bar_plot(df, courses, time):
    sum = [[] for i in range(len(courses))]

    i = 0
    for course in courses:
        for t in time:
            sum[i].append(df.loc[lambda df: df["TIME"] == t][course].sum())
        i = i + 1

    ind = np.arange(len(courses))
    fig, ax = plt.subplots()
    fig.canvas.manager.set_window_title("Bar Plot for each course")
    ax.set_title("Title")
    ax.bar(ind[0], sum[0][0], 0.15, color='r')
    ax.bar(ind[0], sum[0][1], 0.15, bottom=sum[0][0], color='b')
    ax.bar(ind[1], sum[1][0], 0.15, color='r')
    ax.bar(ind[1], sum[1][1], 0.15, bottom=sum[1][0], color='b')
    ax.bar(ind[2], sum[2][0], 0.15, color='r')
    ax.bar(ind[2], sum[2][1], 0.15, bottom=sum[2][0], color='b')
    ax.legend(labels=time)
    plt.xticks(ind, courses)
    fig.savefig(os.path.abspath("../Results/Results_Part1/Bar Plot for each course.png"))


if __name__ == '__main__':
    inputFile = os.path.abspath("../Data/part1.csv")
    outputFile = os.path.abspath("../Results/Results_Part1/output_part1.csv")
    MyDataFrame = pd.read_csv(inputFile, sep=',')

    MyCourses = ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]
    Time = ["LUNCH", "DINNER"]
    # Dish prices sorted in descending order to ease the finding of drink price
    MyPrices = [[20, 15, 3], [40, 25, 20, 9], [15, 10]]

    # Create Plots
    create_dis_plot(MyDataFrame, MyCourses)
    create_bar_plot(MyDataFrame, MyCourses, Time)

    # Calculate dishes and drinks prices
    calculate_drinks(MyDataFrame, MyCourses, MyPrices)
    print("Writing calculation result to " + outputFile)
    MyDataFrame.to_csv(outputFile, sep=',')

    # Show plots
    plt.show()


