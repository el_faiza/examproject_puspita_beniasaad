import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

input1 = pd.read_csv(os.path.abspath("../Results/Results_Part1/output_part1.csv"))
input2 = pd.read_csv(os.path.abspath("../Data/part3.csv"))

price_catalogue = {'Soup': 3.0, 'TomatoMozzarella': 15.0, 'Oysters': 20.0,
        'Salad': 9.0, 'Spaghetti': 20.0, 'Steak': 25.0, 'Lobster': 40.0,
        'IceCream': 15.0, 'Pie': 10.0}

# From data on part 1, the parice of 
max_price_drink = [2.0, 5.0, 2.0]

# Cumulative probability on each column
time_dis_df = pd.DataFrame(data={'Probability': [0.5, 0.5]}, index=['LUNCH', 'DINNER'])
client_dis_df = pd.DataFrame(data={'Probability': [0.11427397260273972, 0.2168219178082192, 0.5186575342465753, 0.15024657534246574]}, index=['Business','Healthy','Onetime','Retirement'])

# Does not represent cumulative probability on each column
returning_probability = {'Business': 0.5, 'Healthy': 0.7, 'Onetime': 0.0, 'Retirement': 0.9}
client_course_probability = pd.DataFrame(data={'COURSE1': [0.990650, 0.959692, 0.094343, 1.0], 'COURSE2': [1.0, 1.0, 1.0, 1.0], 'COURSE3': [0.843922, 0.071772, 0.207543, 0.950401]}, index=['Business','Healthy','Onetime','Retirement'])
client_dish_probability = []
client_dish_probability.append(pd.DataFrame(data={'Soup': [0.0931752178121975, 1.0, 1.0, 0.00784099197665937], 'TomatoMozzarella': [0.0484027105517909, 0.0, 0.0, 0.325309992706054], 'Oysters': [0.858422071636011, 0.0, 0.0, 0.666849015317287]}, index=['Business','Healthy','Onetime','Retirement']))
client_dish_probability.append(pd.DataFrame(data={'Salad': [0.0, 0.99696739954511, 0.0, 0.169401896425966], 'Spaghetti': [0.0, 0.00303260045489007, 0.171200676139665, 0.585703865791393], 'Steak': [0.0, 0.0, 0.664624161428345, 0.24489423778264], 'Lobster': [1.0, 0.0, 0.16417516243199, 0.0]}, index=['Business','Healthy','Onetime','Retirement']))
client_dish_probability.append(pd.DataFrame(data={'Pie': [0.886647727272727, 0.811619718309859, 0.920081445660473, 0.892747505755948], 'IceCream': [0.113352272727273, 0.188380281690141, 0.0799185543395266, 0.107252494244052]}, index=['Business','Healthy','Onetime','Retirement']))


def genID(db):
    val = np.random.random()
    digit = 7  # counts of digit for ID number
    id_return = "ID" + ("{:0"+str(digit)+"d}").format(round(val*pow(10,digit)))
    # If already in database, regenerate the ID
    while id_return in db:
        val = np.random.random()
        id_return = "ID" + ("{:0"+str(digit)+"d}").format(round(val*pow(10,digit)))
    db.append(id_return)
    return id_return


def genRandomNumber():
    val = np.random.random()
    return val


def chooseFromProbDistrib(df):
    rnd_val = genRandomNumber()
    # data must be a cumulative probability
    df = df.cumsum()
    for index, row in df.iterrows():
        if rnd_val <= row['Probability']:
            return index


def getCourse(client_type, dish_prob_df):
    temp_df = dish_prob_df.loc[client_type].sort_values(ascending=True).to_frame().cumsum()
    temp_df = temp_df.rename(columns={client_type: "Probability"})
    return chooseFromProbDistrib(temp_df)


def is_taking_course(client_type, course_i):
    if genRandomNumber() < client_course_probability["COURSE" + str(course_i)].loc[client_type]:
        return True
    else:
        return False


def getReturningId(main_df, client_db, client_type, current_day):
    temp_df = main_df[main_df['CUSTOMERTYPE'] == client_type]
    # Assuming returning client won't return on the same day
    temp_df = temp_df[temp_df['DAY'] < current_day]

    # The is no same client type in the days before today
    if temp_df.empty:
        return genID(client_db)
    # Ensure the randomize id from days before is not duplicated in today's list
    # If duplicate, generate new
    elif temp_df.loc[np.random.choice(temp_df.index)]['CUSTOMERID'] in main_df[main_df['DAY'] == current_day]['CUSTOMERID'].to_list():
        return genID(client_db)
    else:
        return temp_df.loc[np.random.choice(temp_df.index)]['CUSTOMERID']


def is_returning(client_type):
    if genRandomNumber() < returning_probability[client_type]:
        return True
    else:
        return False


def simulateData(main_df, client_db, day):
    client_type = chooseFromProbDistrib(client_dis_df)
    time = chooseFromProbDistrib(time_dis_df)
    if is_returning(client_type):
        client_id = getReturningId(main_df, client_db, client_type, day)
    else:
        client_id = genID(client_db)

    courses = []
    drinks = []
    total = []
    for i in range(3):
        if is_taking_course(client_type, i+1):
            courses.append(getCourse(client_type, client_dish_probability[i]))
            drinks.append(genRandomNumber() * max_price_drink[i])
        else:
            # Not taking course. Assumption that client do not take drink as well
            courses.append("None")
            drinks.append(0.0)

        if courses[i] == "None":
            total.append(0.0)
        else:
            total.append(price_catalogue[courses[i]] + drinks[i])

    generated_df = pd.DataFrame([[day, time, client_id, client_type, courses[0], courses[1], courses[2],
            drinks[0], drinks[1], drinks[2], total[0], total[1], total[2]]],
            columns=['DAY', 'TIME', 'CUSTOMERID', 'CUSTOMERTYPE', 'COURSE1', 'COURSE2', 'COURSE3',
                'DRINK1', 'DRINK2', 'DRINK3', 'TOTAL1', 'TOTAL2', 'TOTAL3'])

    main_df = pd.concat([main_df, generated_df])
    main_df = main_df.reset_index(drop=True)
    return main_df

if __name__ == '__main__':
    client_db = []

    result_df = pd.DataFrame(columns=['DAY', 'TIME', 'CUSTOMERID', 'CUSTOMERTYPE', 'COURSE1', 'COURSE2', 'COURSE3',
                'DRINK1', 'DRINK2', 'DRINK3', 'TOTAL1', 'TOTAL2', 'TOTAL3'])

    dayrange = 365*5  # 5 years range
    course_per_day = 20

    for i in range(dayrange):
        for j in range(course_per_day):
            result_df = simulateData(result_df, client_db, i)

    # Remove column named DAY, because not asked to be presented
    result_df = result_df.drop(columns=['DAY'])
    result_df.to_csv(os.path.abspath("../Results/Results_Part4/output_part4.csv"), sep=',')


#Comparison of output data to the input data
input_df = pd.merge(input1, input2, on='CLIENT_ID')
input_df = input_df.rename(columns={'CLIENT_TYPE': 'CUSTOMERTYPE'})

#Comparaison of Characteristics per group
InputData_Clients_summary = input_df.groupby('CUSTOMERTYPE').describe()
print(InputData_Clients_summary)
OutputData_Clients_summary = result_df.groupby('CUSTOMERTYPE').describe()
print(OutputData_Clients_summary)


#Comparison of clients distribution
Output_data_Clients_dist = result_df.groupby(['CUSTOMERTYPE']).size().reset_index(name='Output_data_Clients_Count')
Output_data_Clients_dist['Output_data_Clients_prob'] =[i/Output_data_Clients_dist['Output_data_Clients_Count'].sum() for i in Output_data_Clients_dist['Output_data_Clients_Count']]
Input_data_Clients_dist = input_df.groupby(['CUSTOMERTYPE']).size().reset_index(name='Input_data_Clients_Count')
Input_data_Clients_dist['Input_data_Clients_prob'] = [i/Input_data_Clients_dist['Input_data_Clients_Count'].sum() for i in Input_data_Clients_dist['Input_data_Clients_Count']]
Compare_Client_dist = pd.merge(Input_data_Clients_dist, Output_data_Clients_dist, on='CUSTOMERTYPE')
Compare_Client_dist['Difference_Clients_prob'] = [i-j for i, j in zip(Compare_Client_dist['Output_data_Clients_prob'],Compare_Client_dist['Input_data_Clients_prob'])]
print(Compare_Client_dist['Difference_Clients_prob'])

#Comparison of The likehood to get a certain course
#Comparison -- first course
subset_df11 = input_df[input_df['FIRST_COURSE'] > 0.0].groupby(['CUSTOMERTYPE']).size().reset_index(name='Starter_Counts_InputData')
subset_df11['Starter_Prob_InputData'] = [i/j for i, j in zip(subset_df11['Starter_Counts_InputData'], Input_data_Clients_dist['Input_data_Clients_Count'])]
subset_df12 = result_df[result_df['COURSE1'] != "None"].groupby(['CUSTOMERTYPE']).size().reset_index(name='Starter_Counts_OutputData')
subset_df12['Starter_Prob_OutputData'] = [i/j for i, j in zip(subset_df12['Starter_Counts_OutputData'], Output_data_Clients_dist['Output_data_Clients_Count'])]
Compare_First_course_dist = pd.merge(subset_df11, subset_df12, on='CUSTOMERTYPE')
Compare_First_course_dist['Difference_Starter_Prob'] = [i-j for i, j in zip(Compare_First_course_dist['Starter_Prob_OutputData'],Compare_First_course_dist['Starter_Prob_InputData'])]
print(Compare_First_course_dist['Difference_Starter_Prob'])

#Comparison -- second course
subset_df21 = input_df[input_df['SECOND_COURSE'] > 0.0].groupby(['CUSTOMERTYPE']).size().reset_index(name='Main_Counts_InputData')
subset_df21['Main_Prob_InputData'] = [i/j for i, j in zip(subset_df21['Main_Counts_InputData'], Input_data_Clients_dist['Input_data_Clients_Count'])]
subset_df22 = result_df[result_df['COURSE2'] != "None"].groupby(['CUSTOMERTYPE']).size().reset_index(name='Main_Counts_OutputData')
subset_df22['Main_Prob_OutputData'] = [i/j for i, j in zip(subset_df22['Main_Counts_OutputData'], Output_data_Clients_dist['Output_data_Clients_Count'])]
Compare_Second_course_dist = pd.merge(subset_df21, subset_df22, on='CUSTOMERTYPE')
Compare_Second_course_dist['Difference_Main_Prob'] = [i-j for i, j in zip(Compare_Second_course_dist['Main_Prob_OutputData'], Compare_Second_course_dist['Main_Prob_InputData'])]
print(Compare_Second_course_dist['Difference_Main_Prob'])

#Comparison -- third course
subset_df31 = input_df[input_df['THIRD_COURSE'] > 0.0].groupby(['CUSTOMERTYPE']).size().reset_index(name='Dessert_Counts_InputData')
subset_df31['Dessert_Prob_InputData'] = [i/j for i, j in zip(subset_df31['Dessert_Counts_InputData'], Input_data_Clients_dist['Input_data_Clients_Count'])]
subset_df32 = result_df[result_df['COURSE3'] != "None"].groupby(['CUSTOMERTYPE']).size().reset_index(name='Dessert_Counts_OutputData')
subset_df32['Dessert_Prob_OutputData'] = [i/j for i, j in zip(subset_df32['Dessert_Counts_OutputData'], Output_data_Clients_dist['Output_data_Clients_Count'])]
Compare_Third_course_dist = pd.merge(subset_df31, subset_df32, on='CUSTOMERTYPE')
Compare_Third_course_dist['Difference_Dessert_Prob'] = [i-j for i, j in zip(Compare_Third_course_dist['Dessert_Prob_OutputData'], Compare_Third_course_dist['Dessert_Prob_InputData'])]
print(Compare_Third_course_dist['Difference_Dessert_Prob'])

#How would The revenue of the restaurant change if they suddenly had twice more certain type of customers
def ChangeInRevenue_For_TwiceMoreCustomers(Client_Type):
    Total_Revenue = result_df[['TOTAL1', 'TOTAL2', 'TOTAL3']].sum().sum() #Calculate the total revenue
    Data_Customers = result_df[result_df['CUSTOMERTYPE'] == Client_Type]
    Revenue_Customers = Data_Customers[['TOTAL1', 'TOTAL2', 'TOTAL3']].sum().sum()  #Calculate the revenue generated from a given type of customers
    ChangeInRevenue = Revenue_Customers/Total_Revenue
    return(ChangeInRevenue)

#How would The revenue of the restaurant change if they suddenly had twice more healthy customers:
print ("if the restaurant suddenly receives twice more healthy customers the revenue will increase by:", ChangeInRevenue_For_TwiceMoreCustomers("Healthy"))

#How would The revenue of the restaurant change if the price of a certain food increased by one unit of money:
def ChangeInRevenue_For_OnechangeInFOOD(FOOD): # only for main course
    Total_Revenue = result_df[['TOTAL1', 'TOTAL2', 'TOTAL3']].sum().sum()  # Calculate the total revenue
    Data_Food = result_df[result_df['COURSE2'] == FOOD]
    Food_count = len(Data_Food)
    ChangeInRevenue = Food_count/Total_Revenue
    return (ChangeInRevenue)

#change in revenue of 2 unit of money increase in the price of spaghetti:
print ("if the price of the spaghetti increase by 2 the revenue will increase by:",2*ChangeInRevenue_For_OnechangeInFOOD("Spaghetti"))



